Compile:  
	mpif90 -ffixed-line-length-150 elecfem3d_mpi.f -o elecfem3d_mpi

Run: 
	mpiexec -np 16 elecfem3d_mpi.exe
	mpirun -n 4 ./elecfem3d_mpi elecfem3d_mpi.pam


- np is the number of nodes. 
- elecfem3d_mpi.pam is the parameter file, 
- elecfem3d_mpi.f is the source code 
- test.dat is the binary image (does not have to be binary)
- testCurrent.dat is the current data in each voxel


