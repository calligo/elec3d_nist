import os,sys,time
import numpy as np

t0 = time.time()

try:
	filename = sys.argv[1]
	if not filename.endswith(".raw"):
		sys.exit("\nERROR :: specified file must be a .raw file\n")
except:
	sys.exit("\nERROR :: must specify .raw file to convert\n")

try:
	base = sys.argv[2]
except:
	base = filename.split(".raw")[0]

print("\nLoading .raw ...")
a = np.fromfile(filename,'uint8')
a = np.clip(a,0,1)

dat_name = "%s.dat" % (base)

if not os.path.exists(dat_name):
	print("\nSaving .dat ...")
	np.savetxt(dat_name,a,fmt='%s',delimiter='\n')
else:
	sys.exit("\nERROR :: .dat file already exists\n")

t1 = time.time()
minutes = int((t1-t0)/60)

sys.exit("\nFINISHED :: good job!... took %s minutes\n" % (minutes))
